// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

//! Project variable API endpoints.
//!
//! These endpoints are used for querying a project's variables.

mod create;
mod delete;
mod update;
mod variable;
mod variables;

pub use self::create::CreateGroupVariable;
pub use self::create::CreateGroupVariableBuilder;
pub use self::create::CreateGroupVariableBuilderError;
pub use self::create::GroupVariableType;

pub use self::delete::DeleteGroupVariable;
pub use self::delete::DeleteGroupVariableBuilder;
pub use self::delete::DeleteGroupVariableBuilderError;

pub use self::update::UpdateGroupVariable;
pub use self::update::UpdateGroupVariableBuilder;
pub use self::update::UpdateGroupVariableBuilderError;

pub use self::variable::GroupVariable;
pub use self::variable::GroupVariableBuilder;
pub use self::variable::GroupVariableBuilderError;
pub use self::variable::GroupVariableFilter;

pub use self::variables::GroupVariables;
pub use self::variables::GroupVariablesBuilder;
pub use self::variables::GroupVariablesBuilderError;
