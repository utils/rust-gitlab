// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use derive_builder::Builder;

use crate::api::common::{self, NameOrId};
use crate::api::endpoint_prelude::*;
use crate::api::groups::variables::GroupVariableFilter;

/// Delete a variable of a group.
#[derive(Debug, Builder, Clone)]
#[builder(setter(strip_option))]
pub struct DeleteGroupVariable<'a> {
    /// The group to remove the variable from
    #[builder(setter(into))]
    group: NameOrId<'a>,
    /// The name of the variable.
    #[builder(setter(into))]
    key: Cow<'a, str>,
    /// The environment scope filter of the variable.
    #[builder(default)]
    filter: Option<GroupVariableFilter<'a>>,
}

impl<'a> DeleteGroupVariable<'a> {
    /// Create a builder for the endpoint.
    pub fn builder() -> DeleteGroupVariableBuilder<'a> {
        DeleteGroupVariableBuilder::default()
    }
}

impl Endpoint for DeleteGroupVariable<'_> {
    fn method(&self) -> Method {
        Method::DELETE
    }

    fn endpoint(&self) -> Cow<'static, str> {
        format!(
            "groups/{}/variables/{}",
            self.group,
            common::path_escaped(&self.key),
        )
        .into()
    }

    fn body(&self) -> Result<Option<(&'static str, Vec<u8>)>, BodyError> {
        let mut params = FormParams::default();

        if let Some(filter) = self.filter.as_ref() {
            filter.add_query(&mut params);
        }

        params.into_body()
    }
}

#[cfg(test)]
mod tests {
    use http::Method;

    use crate::api::groups::variables::delete::{
        DeleteGroupVariable, DeleteGroupVariableBuilderError, GroupVariableFilter,
    };
    use crate::api::{self, Query};
    use crate::test::client::{ExpectedUrl, SingleTestClient};

    #[test]
    fn all_parameters_are_needed() {
        let err = DeleteGroupVariable::builder().build().unwrap_err();
        crate::test::assert_missing_field!(err, DeleteGroupVariableBuilderError, "group");
    }

    #[test]
    fn group_is_necessary() {
        let err = DeleteGroupVariable::builder()
            .key("testkey")
            .build()
            .unwrap_err();
        crate::test::assert_missing_field!(err, DeleteGroupVariableBuilderError, "group");
    }

    #[test]
    fn key_is_necessary() {
        let err = DeleteGroupVariable::builder().group(1).build().unwrap_err();
        crate::test::assert_missing_field!(err, DeleteGroupVariableBuilderError, "key");
    }

    #[test]
    fn sufficient_parameters() {
        DeleteGroupVariable::builder()
            .group(1)
            .key("testkey")
            .build()
            .unwrap();
    }

    #[test]
    fn endpoint() {
        let endpoint = ExpectedUrl::builder()
            .method(Method::DELETE)
            .endpoint("groups/simple%2Fgroup/variables/testkey")
            .content_type("application/x-www-form-urlencoded")
            .build()
            .unwrap();
        let client = SingleTestClient::new_raw(endpoint, "");

        let endpoint = DeleteGroupVariable::builder()
            .group("simple/group")
            .key("testkey")
            .build()
            .unwrap();
        api::ignore(endpoint).query(&client).unwrap();
    }

    #[test]
    fn endpoint_filter() {
        let endpoint = ExpectedUrl::builder()
            .method(Method::DELETE)
            .endpoint("groups/simple%2Fgroup/variables/testkey")
            .content_type("application/x-www-form-urlencoded")
            .body_str("filter%5Benvironment_scope%5D=production")
            .build()
            .unwrap();
        let client = SingleTestClient::new_raw(endpoint, "");

        let endpoint = DeleteGroupVariable::builder()
            .group("simple/group")
            .key("testkey")
            .filter(
                GroupVariableFilter::builder()
                    .environment_scope("production")
                    .build()
                    .unwrap(),
            )
            .build()
            .unwrap();
        api::ignore(endpoint).query(&client).unwrap();
    }
}
