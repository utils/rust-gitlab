// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use std::borrow::Cow;
use std::collections::BTreeSet;

use crate::api::{ParamValue, QueryParams};

/// Filters for merge request states.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[non_exhaustive]
pub enum MergeRequestState {
    /// Filter merge requests that are open.
    Opened,
    /// Filter merge requests that are closed.
    Closed,
    /// Filter merge requests that are locked.
    Locked,
    /// Filter merge requests that are merged.
    Merged,
}

impl MergeRequestState {
    fn as_str(self) -> &'static str {
        match self {
            MergeRequestState::Opened => "opened",
            MergeRequestState::Closed => "closed",
            MergeRequestState::Locked => "locked",
            MergeRequestState::Merged => "merged",
        }
    }
}

impl ParamValue<'static> for MergeRequestState {
    fn as_value(&self) -> Cow<'static, str> {
        self.as_str().into()
    }
}

/// Parameters for a merge request view.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[non_exhaustive]
pub enum MergeRequestView {
    /// Return just the IID, URL, title, description, and basic state information.
    Simple,
}

impl MergeRequestView {
    fn as_str(self) -> &'static str {
        match self {
            MergeRequestView::Simple => "simple",
        }
    }
}

impl ParamValue<'static> for MergeRequestView {
    fn as_value(&self) -> Cow<'static, str> {
        self.as_str().into()
    }
}

/// Filter merge requests by a scope.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[non_exhaustive]
pub enum MergeRequestScope {
    /// Filter merge requests created by the API caller.
    CreatedByMe,
    /// Filter merge requests assigned to the API caller.
    AssignedToMe,
    /// Return all merge requests.
    All,
}

impl MergeRequestScope {
    fn as_str(self) -> &'static str {
        match self {
            MergeRequestScope::CreatedByMe => "created_by_me",
            MergeRequestScope::AssignedToMe => "assigned_to_me",
            MergeRequestScope::All => "all",
        }
    }
}

impl ParamValue<'static> for MergeRequestScope {
    fn as_value(&self) -> Cow<'static, str> {
        self.as_str().into()
    }
}

/// Filters for merge request milestones.
#[derive(Debug, Clone)]
#[non_exhaustive]
pub enum MergeRequestMilestone<'a> {
    /// Filter merge requests with no milestones.
    None,
    /// Filter merge requests with any milestone.
    Any,
    /// Filter merge reqeusts for a specific milestone.
    Named(Cow<'a, str>),
}

impl MergeRequestMilestone<'_> {
    fn as_str(&self) -> &str {
        match self {
            MergeRequestMilestone::None => "None",
            MergeRequestMilestone::Any => "Any",
            MergeRequestMilestone::Named(name) => name.as_ref(),
        }
    }
}

impl<'a, 'b: 'a> ParamValue<'a> for &'b MergeRequestMilestone<'a> {
    fn as_value(&self) -> Cow<'a, str> {
        self.as_str().into()
    }
}

/// Keys merge request results may be ordered by.
#[derive(Debug, Default, Clone, Copy, PartialEq, Eq)]
#[non_exhaustive]
pub enum MergeRequestOrderBy {
    /// Sort by creation date.
    #[default]
    CreatedAt,
    /// Sort by merge date.
    MergedAt,
    /// Sort by last updated date.
    UpdatedAt,
    /// Sort by title.
    Title,
}

impl MergeRequestOrderBy {
    fn as_str(self) -> &'static str {
        match self {
            MergeRequestOrderBy::CreatedAt => "created_at",
            MergeRequestOrderBy::MergedAt => "merged_at",
            MergeRequestOrderBy::UpdatedAt => "updated_at",
            MergeRequestOrderBy::Title => "title",
        }
    }
}

impl ParamValue<'static> for MergeRequestOrderBy {
    fn as_value(&self) -> Cow<'static, str> {
        self.as_str().into()
    }
}

/// The scope to apply search query terms to.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum MergeRequestSearchScope {
    /// Search within titles.
    Title,
    /// Search within descriptions.
    Description,
}

impl MergeRequestSearchScope {
    fn as_str(self) -> &'static str {
        match self {
            MergeRequestSearchScope::Title => "title",
            MergeRequestSearchScope::Description => "description",
        }
    }
}

impl ParamValue<'static> for MergeRequestSearchScope {
    fn as_value(&self) -> Cow<'static, str> {
        self.as_str().into()
    }
}

/// Assignee for a merge request.
#[derive(Debug, Clone)]
#[non_exhaustive]
pub enum Assignee {
    /// Assigned to anyone
    Assigned,
    /// Assigned to no one.
    Unassigned,
    /// Assigned to a specific user.
    Id(u64),
}

impl Assignee {
    pub(crate) fn add_params<'a>(&'a self, params: &mut QueryParams<'a>) {
        match self {
            Assignee::Assigned => {
                params.push("assignee_id", "Any");
            },
            Assignee::Unassigned => {
                params.push("assignee_id", "None");
            },
            Assignee::Id(id) => {
                params.push("assignee_id", *id);
            },
        }
    }
}

/// Filter merge requests by approvers.
#[derive(Debug, Clone)]
#[non_exhaustive]
pub enum ApproverIds {
    /// Filter merge requests with any approvers.
    Any,
    /// Filter merge requests with no approvers
    None,
    /// Filter merge requests by specific approvers.
    AllOf(BTreeSet<u64>),
}

impl ApproverIds {
    pub(crate) fn add_params<'a>(&'a self, params: &mut QueryParams<'a>) {
        match self {
            ApproverIds::Any => {
                params.push("approver_ids", "Any");
            },
            ApproverIds::None => {
                params.push("approver_ids", "None");
            },
            ApproverIds::AllOf(ids) => {
                params.extend(ids.iter().map(|&id| ("approver_ids[]", id)));
            },
        }
    }
}

#[cfg(test)]
mod tests {
    use super::{
        MergeRequestMilestone, MergeRequestOrderBy, MergeRequestScope, MergeRequestState,
        MergeRequestView,
    };

    #[test]
    fn merge_request_state_as_str() {
        let items = &[
            (MergeRequestState::Opened, "opened"),
            (MergeRequestState::Closed, "closed"),
            (MergeRequestState::Locked, "locked"),
            (MergeRequestState::Merged, "merged"),
        ];

        for (i, s) in items {
            assert_eq!(i.as_str(), *s);
        }
    }

    #[test]
    fn merge_request_view_as_str() {
        let items = &[(MergeRequestView::Simple, "simple")];

        for (i, s) in items {
            assert_eq!(i.as_str(), *s);
        }
    }

    #[test]
    fn merge_request_scope_as_str() {
        let items = &[
            (MergeRequestScope::CreatedByMe, "created_by_me"),
            (MergeRequestScope::AssignedToMe, "assigned_to_me"),
            (MergeRequestScope::All, "all"),
        ];

        for (i, s) in items {
            assert_eq!(i.as_str(), *s);
        }
    }

    #[test]
    fn merge_request_milestone_as_str() {
        let items = &[
            (MergeRequestMilestone::Any, "Any"),
            (MergeRequestMilestone::None, "None"),
            (
                MergeRequestMilestone::Named("milestone".into()),
                "milestone",
            ),
        ];

        for (i, s) in items {
            assert_eq!(i.as_str(), *s);
        }
    }

    #[test]
    fn merge_request_order_by_default() {
        assert_eq!(
            MergeRequestOrderBy::default(),
            MergeRequestOrderBy::CreatedAt,
        );
    }

    #[test]
    fn merge_request_order_by_as_str() {
        let items = &[
            (MergeRequestOrderBy::CreatedAt, "created_at"),
            (MergeRequestOrderBy::MergedAt, "merged_at"),
            (MergeRequestOrderBy::UpdatedAt, "updated_at"),
            (MergeRequestOrderBy::Title, "title"),
        ];

        for (i, s) in items {
            assert_eq!(i.as_str(), *s);
        }
    }
}
