// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

mod link_header;
mod pagination;

mod all_at_once;
mod lazy;

/// A trait to indicate that an endpoint is pageable.
pub trait Pageable {
    /// Whether the endpoint uses keyset pagination or not.
    fn use_keyset_pagination(&self) -> bool {
        false
    }
}

impl<E> Pageable for &E
where
    E: Pageable,
{
    fn use_keyset_pagination(&self) -> bool {
        (*self).use_keyset_pagination()
    }
}

pub use self::link_header::LinkHeaderParseError;

pub use self::pagination::Pagination;
pub use self::pagination::PaginationError;

// pub use self::page::page;
// pub use self::page::Page;

pub use self::all_at_once::paged;
pub use self::all_at_once::Paged;

pub use self::lazy::LazilyPagedIter;

#[cfg(test)]
mod tests {
    use crate::api::{self, Pageable};

    #[test]
    fn test_pageable_ref_use_keyset_pagination() {
        let with_keyset = api::projects::jobs::Jobs::builder()
            .project(1)
            .build()
            .unwrap();
        let without_keyset = api::projects::deploy_keys::DeployKeys::builder()
            .project(1)
            .build()
            .unwrap();
        let with_keyset_ref = &with_keyset;
        let without_keyset_ref = &without_keyset;

        assert!(with_keyset_ref.use_keyset_pagination());
        assert!(!without_keyset_ref.use_keyset_pagination());
    }
}
