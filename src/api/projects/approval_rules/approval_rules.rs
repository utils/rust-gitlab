// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use derive_builder::Builder;

use crate::api::common::NameOrId;
use crate::api::endpoint_prelude::*;

/// Query for approval rules of a project.
/// See https://docs.gitlab.com/ee/api/merge_request_approvals.html#get-project-level-rules
#[derive(Debug, Builder, Clone)]
pub struct ProjectApprovalRules<'a> {
    /// The project to query for approval rules.
    #[builder(setter(into))]
    project: NameOrId<'a>,
}

impl<'a> ProjectApprovalRules<'a> {
    /// Create a builder for the endpoint.
    pub fn builder() -> ProjectApprovalRulesBuilder<'a> {
        ProjectApprovalRulesBuilder::default()
    }
}

impl Endpoint for ProjectApprovalRules<'_> {
    fn method(&self) -> Method {
        Method::GET
    }

    fn endpoint(&self) -> Cow<'static, str> {
        format!("projects/{}/approval_rules", self.project).into()
    }
}

impl Pageable for ProjectApprovalRules<'_> {}

#[cfg(test)]
mod tests {
    use crate::api::projects::approval_rules::{
        ProjectApprovalRules, ProjectApprovalRulesBuilderError,
    };
    use crate::api::{self, Query};
    use crate::test::client::{ExpectedUrl, SingleTestClient};

    #[test]
    fn project_is_needed() {
        let err = ProjectApprovalRules::builder().build().unwrap_err();
        crate::test::assert_missing_field!(err, ProjectApprovalRulesBuilderError, "project");
    }

    #[test]
    fn project_is_sufficient() {
        ProjectApprovalRules::builder().project(1).build().unwrap();
    }

    #[test]
    fn endpoint() {
        let endpoint = ExpectedUrl::builder()
            .endpoint("projects/simple%2Fproject/approval_rules")
            .build()
            .unwrap();
        let client = SingleTestClient::new_raw(endpoint, "");

        let endpoint = ProjectApprovalRules::builder()
            .project("simple/project")
            .build()
            .unwrap();
        api::ignore(endpoint).query(&client).unwrap();
    }
}
