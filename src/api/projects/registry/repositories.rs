// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use derive_builder::Builder;

use crate::api::common::NameOrId;
use crate::api::endpoint_prelude::*;

/// Query for registry repositories within a project.
#[derive(Debug, Builder, Clone)]
#[builder(setter(strip_option))]
pub struct Repositories<'a> {
    /// The project to query for repositories.
    #[builder(setter(into))]
    project: NameOrId<'a>,
}

impl<'a> Repositories<'a> {
    /// Create a builder for the endpoint.
    pub fn builder() -> RepositoriesBuilder<'a> {
        RepositoriesBuilder::default()
    }
}

impl Endpoint for Repositories<'_> {
    fn method(&self) -> Method {
        Method::GET
    }

    fn endpoint(&self) -> Cow<'static, str> {
        format!("projects/{}/registry/repositories", self.project).into()
    }
}

impl Pageable for Repositories<'_> {}

#[cfg(test)]
mod tests {
    use crate::api::projects::registry::{Repositories, RepositoriesBuilderError};
    use crate::api::{self, Query};
    use crate::test::client::{ExpectedUrl, SingleTestClient};

    #[test]
    fn project_is_necessary() {
        let err = Repositories::builder().build().unwrap_err();
        crate::test::assert_missing_field!(err, RepositoriesBuilderError, "project");
    }

    #[test]
    fn project_is_sufficient() {
        Repositories::builder().project(1).build().unwrap();
    }

    #[test]
    fn endpoint() {
        let endpoint = ExpectedUrl::builder()
            .endpoint("projects/simple%2Fproject/registry/repositories")
            .build()
            .unwrap();
        let client = SingleTestClient::new_raw(endpoint, "");

        let endpoint = Repositories::builder()
            .project("simple/project")
            .build()
            .unwrap();
        api::ignore(endpoint).query(&client).unwrap();
    }
}
