//! Copyright (c) Microsoft Corporation. All rights reserved.
//! Highly Confidential Material
use derive_builder::Builder;

use crate::api::common::{NameOrId, SortOrder};
use crate::api::endpoint_prelude::*;
use crate::api::ParamValue;

/// Keys contributor results may be ordered by.
#[derive(Debug, Default, Clone, Copy, PartialEq, Eq)]
#[non_exhaustive]
pub enum ContributorsOrderBy {
    /// Order by the user name.
    Name,
    /// Order by email.
    Email,
    /// Order by most recent commit.
    #[default]
    Commits,
}

impl ContributorsOrderBy {
    /// The ordering as a query parameter.
    fn as_str(self) -> &'static str {
        match self {
            ContributorsOrderBy::Name => "name",
            ContributorsOrderBy::Email => "email",
            ContributorsOrderBy::Commits => "commits",
        }
    }
}

impl ParamValue<'static> for ContributorsOrderBy {
    fn as_value(&self) -> Cow<'static, str> {
        self.as_str().into()
    }
}

/// Query for repository contributors.
#[derive(Debug, Builder, Clone)]
#[builder(setter(strip_option))]
pub struct Contributors<'a> {
    /// The project to get contributors from.
    #[builder(setter(into))]
    project: NameOrId<'a>,
    /// The name of the reference to get contributors of.
    #[builder(setter(into), default)]
    ref_: Option<Cow<'a, str>>,

    /// Return contributors ordered by keys.
    #[builder(default)]
    order_by: Option<ContributorsOrderBy>,
    /// The sort order for returned results.
    #[builder(default)]
    sort: Option<SortOrder>,
}

impl<'a> Contributors<'a> {
    /// Create a builder for the endpoint.
    pub fn builder() -> ContributorsBuilder<'a> {
        ContributorsBuilder::default()
    }
}

impl Endpoint for Contributors<'_> {
    fn method(&self) -> Method {
        Method::GET
    }

    fn endpoint(&self) -> Cow<'static, str> {
        format!("projects/{}/repository/contributors", self.project).into()
    }

    fn parameters(&self) -> QueryParams {
        let mut params = QueryParams::default();

        params
            .push_opt("sort", self.sort)
            .push_opt("order_by", self.order_by)
            .push_opt("ref", self.ref_.as_ref());

        params
    }
}

impl Pageable for Contributors<'_> {}

#[cfg(test)]
mod tests {
    use crate::{
        api::{
            common::SortOrder,
            projects::repository::contributors::{
                Contributors, ContributorsBuilderError, ContributorsOrderBy,
            },
            Query,
        },
        test::client::{ExpectedUrl, SingleTestClient},
    };

    #[test]
    fn project_is_needed() {
        let err = Contributors::builder().build().unwrap_err();
        crate::test::assert_missing_field!(err, ContributorsBuilderError, "project");
    }

    #[test]
    fn project_is_sufficient() {
        Contributors::builder().project(1).build().unwrap();
    }

    #[test]
    fn endpoint() {
        let endpoint = ExpectedUrl::builder()
            .endpoint("projects/simple%2Fproject/repository/contributors")
            .build()
            .unwrap();
        let client = SingleTestClient::new_raw(endpoint, "");

        let endpoint = Contributors::builder()
            .project("simple/project")
            .build()
            .unwrap();
        crate::api::ignore(endpoint).query(&client).unwrap();
    }

    #[test]
    fn default_order_by() {
        assert_eq!(ContributorsOrderBy::default(), ContributorsOrderBy::Commits);
    }

    #[test]
    fn order_by_as_str() {
        assert_eq!(ContributorsOrderBy::Name.as_str(), "name");
        assert_eq!(ContributorsOrderBy::Email.as_str(), "email");
        assert_eq!(ContributorsOrderBy::Commits.as_str(), "commits");
    }

    #[test]
    fn endpoint_order_by() {
        let endpoint = ExpectedUrl::builder()
            .endpoint("projects/simple%2Fproject/repository/contributors")
            .add_query_params(&[("order_by", "name")])
            .build()
            .unwrap();
        let client = SingleTestClient::new_raw(endpoint, "");

        let endpoint = Contributors::builder()
            .project("simple/project")
            .order_by(ContributorsOrderBy::Name)
            .build()
            .unwrap();
        crate::api::ignore(endpoint).query(&client).unwrap();
    }

    #[test]
    fn endpoint_sort() {
        let endpoint = ExpectedUrl::builder()
            .endpoint("projects/simple%2Fproject/repository/contributors")
            .add_query_params(&[("sort", "asc")])
            .build()
            .unwrap();
        let client = SingleTestClient::new_raw(endpoint, "");

        let endpoint = Contributors::builder()
            .project("simple/project")
            .sort(SortOrder::Ascending)
            .build()
            .unwrap();
        crate::api::ignore(endpoint).query(&client).unwrap();
    }

    #[test]
    fn endpoint_ref() {
        let endpoint = ExpectedUrl::builder()
            .endpoint("projects/simple%2Fproject/repository/contributors")
            .add_query_params(&[("ref", "branch")])
            .build()
            .unwrap();
        let client = SingleTestClient::new_raw(endpoint, "");

        let endpoint = Contributors::builder()
            .project("simple/project")
            .ref_("branch")
            .build()
            .unwrap();
        crate::api::ignore(endpoint).query(&client).unwrap();
    }
}
