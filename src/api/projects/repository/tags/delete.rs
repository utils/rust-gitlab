// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use derive_builder::Builder;

use crate::api::common::NameOrId;
use crate::api::endpoint_prelude::*;

/// Delete a tag on a project.
#[derive(Debug, Builder, Clone)]
pub struct DeleteTag<'a> {
    /// The project to delete a tag on.
    #[builder(setter(into))]
    project: NameOrId<'a>,
    /// The name of the tag.
    #[builder(setter(into))]
    tag: Cow<'a, str>,
}

impl<'a> DeleteTag<'a> {
    /// Create a builder for the endpoint.
    pub fn builder() -> DeleteTagBuilder<'a> {
        DeleteTagBuilder::default()
    }
}

impl Endpoint for DeleteTag<'_> {
    fn method(&self) -> Method {
        Method::DELETE
    }

    fn endpoint(&self) -> Cow<'static, str> {
        format!("projects/{}/repository/tags/{}", self.project, self.tag).into()
    }
}

#[cfg(test)]
mod tests {
    use http::Method;

    use crate::api::projects::repository::tags::{DeleteTag, DeleteTagBuilderError};
    use crate::api::{self, Query};
    use crate::test::client::{ExpectedUrl, SingleTestClient};

    #[test]
    fn project_is_necessary() {
        let err = DeleteTag::builder().tag("mytag").build().unwrap_err();
        crate::test::assert_missing_field!(err, DeleteTagBuilderError, "project");
    }

    #[test]
    fn tag_is_necessary() {
        let err = DeleteTag::builder()
            .project("test/project")
            .build()
            .unwrap_err();
        crate::test::assert_missing_field!(err, DeleteTagBuilderError, "tag");
    }

    #[test]
    fn project_and_tag_are_sufficient() {
        DeleteTag::builder()
            .project(1)
            .tag("mytag")
            .build()
            .unwrap();
    }

    #[test]
    fn endpoint() {
        let endpoint = ExpectedUrl::builder()
            .method(Method::DELETE)
            .endpoint("projects/simple%2Fproject/repository/tags/mytag")
            .build()
            .unwrap();

        let client = SingleTestClient::new_raw(endpoint, "");

        let endpoint = DeleteTag::builder()
            .project("simple/project")
            .tag("mytag")
            .build()
            .unwrap();
        api::ignore(endpoint).query(&client).unwrap();
    }
}
