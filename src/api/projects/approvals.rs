// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

//! Project approvals configuration API endpoint.
//!
//! See <https://docs.gitlab.com/ee/api/merge_request_approvals.html#project-level-mr-approvals>
mod approvals;

pub use self::approvals::ProjectApprovals;
pub use self::approvals::ProjectApprovalsBuilder;
pub use self::approvals::ProjectApprovalsBuilderError;
