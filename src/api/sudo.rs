// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use crate::api::endpoint_prelude::*;

/// A `sudo` modifier that can be applied to any endpoint.
#[derive(Debug, Clone)]
pub struct SudoContext<'a> {
    /// The username to use for the endpoint.
    sudo: Cow<'a, str>,
}

impl<'a> SudoContext<'a> {
    /// Create a new `sudo` context for API endpoints.
    pub fn new<S>(sudo: S) -> Self
    where
        S: Into<Cow<'a, str>>,
    {
        SudoContext {
            sudo: sudo.into(),
        }
    }

    /// Apply the context to an endpoint.
    pub fn apply<E>(&self, endpoint: E) -> Sudo<'a, E> {
        Sudo {
            endpoint,
            sudo: self.sudo.clone(),
        }
    }
}

/// Query information about the API calling user.
#[derive(Debug, Clone)]
pub struct Sudo<'a, E> {
    /// The endpoint to call with `sudo`.
    endpoint: E,

    /// The username to use for the endpoint.
    sudo: Cow<'a, str>,
}

/// Create a `sudo`-elevated version of an endpoint.
pub fn sudo<'a, E, S>(endpoint: E, sudo: S) -> Sudo<'a, E>
where
    S: Into<Cow<'a, str>>,
{
    Sudo {
        endpoint,
        sudo: sudo.into(),
    }
}

impl<E> Endpoint for Sudo<'_, E>
where
    E: Endpoint,
{
    fn method(&self) -> Method {
        self.endpoint.method()
    }

    fn endpoint(&self) -> Cow<'static, str> {
        self.endpoint.endpoint()
    }

    fn parameters(&self) -> QueryParams {
        let mut params = self.endpoint.parameters();
        params.push("sudo", &self.sudo);
        params
    }

    fn body(&self) -> Result<Option<(&'static str, Vec<u8>)>, BodyError> {
        self.endpoint.body()
    }
}

impl<E> Pageable for Sudo<'_, E>
where
    E: Pageable,
{
    fn use_keyset_pagination(&self) -> bool {
        self.endpoint.use_keyset_pagination()
    }
}

#[cfg(test)]
mod tests {
    use http::StatusCode;
    use serde::Deserialize;
    use serde_json::json;
    use url::Url;

    use crate::api::endpoint_prelude::*;
    use crate::api::{self, ApiError, Query, SudoContext};
    use crate::test::client::{ExpectedUrl, SingleTestClient};

    struct Dummy;

    impl Endpoint for Dummy {
        fn method(&self) -> Method {
            Method::GET
        }

        fn endpoint(&self) -> Cow<'static, str> {
            "dummy".into()
        }
    }

    #[derive(Debug, Deserialize)]
    struct DummyResult {
        value: u8,
    }

    #[test]
    fn test_gitlab_non_json_response() {
        let endpoint = ExpectedUrl::builder()
            .endpoint("dummy")
            .add_query_params(&[("sudo", "user")])
            .build()
            .unwrap();
        let client = SingleTestClient::new_raw(endpoint, "not json");

        let res: Result<DummyResult, _> = api::sudo(Dummy, "user").query(&client);
        let err = res.unwrap_err();
        if let ApiError::GitlabService {
            status, ..
        } = err
        {
            assert_eq!(status, StatusCode::OK);
        } else {
            panic!("unexpected error: {}", err);
        }
    }

    #[test]
    fn test_gitlab_empty_response() {
        let endpoint = ExpectedUrl::builder()
            .endpoint("dummy")
            .add_query_params(&[("sudo", "user")])
            .build()
            .unwrap();
        let client = SingleTestClient::new_raw(endpoint, "");

        let res: Result<DummyResult, _> = api::sudo(Dummy, "user").query(&client);
        let err = res.unwrap_err();
        if let ApiError::GitlabService {
            status, ..
        } = err
        {
            assert_eq!(status, StatusCode::OK);
        } else {
            panic!("unexpected error: {}", err);
        }
    }

    #[test]
    fn test_gitlab_error_bad_json() {
        let endpoint = ExpectedUrl::builder()
            .endpoint("dummy")
            .add_query_params(&[("sudo", "user")])
            .status(StatusCode::NOT_FOUND)
            .build()
            .unwrap();
        let client = SingleTestClient::new_raw(endpoint, "");

        let res: Result<DummyResult, _> = api::sudo(Dummy, "user").query(&client);
        let err = res.unwrap_err();
        if let ApiError::GitlabService {
            status, ..
        } = err
        {
            assert_eq!(status, StatusCode::NOT_FOUND);
        } else {
            panic!("unexpected error: {}", err);
        }
    }

    #[test]
    fn test_gitlab_error_detection() {
        let endpoint = ExpectedUrl::builder()
            .endpoint("dummy")
            .add_query_params(&[("sudo", "user")])
            .status(StatusCode::NOT_FOUND)
            .build()
            .unwrap();
        let client = SingleTestClient::new_json(
            endpoint,
            &json!({
                "message": "dummy error message",
            }),
        );

        let res: Result<DummyResult, _> = api::sudo(Dummy, "user").query(&client);
        let err = res.unwrap_err();
        if let ApiError::GitlabWithStatus {
            status,
            msg,
        } = err
        {
            assert_eq!(status, StatusCode::NOT_FOUND);
            assert_eq!(msg, "dummy error message");
        } else {
            panic!("unexpected error: {}", err);
        }
    }

    #[test]
    fn test_gitlab_error_detection_legacy() {
        let endpoint = ExpectedUrl::builder()
            .endpoint("dummy")
            .add_query_params(&[("sudo", "user")])
            .status(StatusCode::NOT_FOUND)
            .build()
            .unwrap();
        let client = SingleTestClient::new_json(
            endpoint,
            &json!({
                "error": "dummy error message",
            }),
        );

        let res: Result<DummyResult, _> = api::sudo(Dummy, "user").query(&client);
        let err = res.unwrap_err();
        if let ApiError::GitlabWithStatus {
            status,
            msg,
        } = err
        {
            assert_eq!(status, StatusCode::NOT_FOUND);
            assert_eq!(msg, "dummy error message");
        } else {
            panic!("unexpected error: {}", err);
        }
    }

    #[test]
    fn test_gitlab_error_detection_unknown() {
        let endpoint = ExpectedUrl::builder()
            .endpoint("dummy")
            .add_query_params(&[("sudo", "user")])
            .status(StatusCode::NOT_FOUND)
            .build()
            .unwrap();
        let err_obj = json!({
            "bogus": "dummy error message",
        });
        let client = SingleTestClient::new_json(endpoint, &err_obj);

        let res: Result<DummyResult, _> = api::sudo(Dummy, "user").query(&client);
        let err = res.unwrap_err();
        if let ApiError::GitlabUnrecognizedWithStatus {
            status,
            obj,
        } = err
        {
            assert_eq!(status, StatusCode::NOT_FOUND);
            assert_eq!(obj, err_obj);
        } else {
            panic!("unexpected error: {}", err);
        }
    }

    #[test]
    fn test_bad_deserialization() {
        let endpoint = ExpectedUrl::builder()
            .endpoint("dummy")
            .add_query_params(&[("sudo", "user")])
            .build()
            .unwrap();
        let client = SingleTestClient::new_json(
            endpoint,
            &json!({
                "not_value": 0,
            }),
        );

        let res: Result<DummyResult, _> = api::sudo(Dummy, "user").query(&client);
        let err = res.unwrap_err();
        if let ApiError::DataType {
            source,
            typename,
        } = err
        {
            assert_eq!(source.to_string(), "missing field `value`");
            assert_eq!(typename, "gitlab::api::sudo::tests::DummyResult");
        } else {
            panic!("unexpected error: {}", err);
        }
    }

    #[test]
    fn test_good_deserialization() {
        let endpoint = ExpectedUrl::builder()
            .endpoint("dummy")
            .add_query_params(&[("sudo", "user")])
            .build()
            .unwrap();
        let client = SingleTestClient::new_json(
            endpoint,
            &json!({
                "value": 0,
            }),
        );

        let res: DummyResult = api::sudo(Dummy, "user").query(&client).unwrap();
        assert_eq!(res.value, 0);
    }

    #[test]
    fn test_sudo_context() {
        let endpoint = ExpectedUrl::builder()
            .endpoint("dummy")
            .add_query_params(&[("sudo", "user")])
            .build()
            .unwrap();
        let client = SingleTestClient::new_json(
            endpoint,
            &json!({
                "value": 0,
            }),
        );
        let sudo_ctx = SudoContext::new("user");
        let endpoint = sudo_ctx.apply(Dummy);

        let res: DummyResult = endpoint.query(&client).unwrap();
        assert_eq!(res.value, 0);
    }

    #[test]
    fn test_sudo_use_keyset_pagination() {
        let with_keyset = api::projects::jobs::Jobs::builder()
            .project(1)
            .build()
            .unwrap();
        let without_keyset = api::projects::deploy_keys::DeployKeys::builder()
            .project(1)
            .build()
            .unwrap();

        assert!(api::sudo(with_keyset, "user").use_keyset_pagination());
        assert!(!api::sudo(without_keyset, "user").use_keyset_pagination());
    }

    struct DummyWithParameters;

    impl Endpoint for DummyWithParameters {
        fn method(&self) -> Method {
            Method::GET
        }

        fn endpoint(&self) -> Cow<'static, str> {
            "dummy/params".into()
        }

        fn parameters(&self) -> QueryParams {
            let mut params = QueryParams::default();
            params.push("dummy", true);
            params
        }

        fn body(&self) -> Result<Option<(&'static str, Vec<u8>)>, BodyError> {
            let mut params = FormParams::default();
            params.push("dummy", true);
            params.into_body()
        }
    }

    #[test]
    fn test_sudo_parameters() {
        let endpoint = DummyWithParameters;
        let sudo_endpoint = api::sudo(endpoint, "user");

        let params = sudo_endpoint.parameters();
        let mut url = Url::parse("https://example.com").unwrap();
        params.add_to_url(&mut url);
        itertools::assert_equal(
            url.query_pairs(),
            [
                ("dummy".into(), "true".into()),
                ("sudo".into(), "user".into()),
            ],
        );
    }

    #[test]
    fn test_sudo_body() {
        let endpoint = DummyWithParameters;
        let sudo_endpoint = api::sudo(endpoint, "user");

        let params = sudo_endpoint.body().unwrap();
        if let Some((mime, bytes)) = params {
            assert_eq!(mime, "application/x-www-form-urlencoded");
            assert_eq!(bytes, b"dummy=true");
        } else {
            panic!("expected to have `dummy=true`");
        }
    }
}
