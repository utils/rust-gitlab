// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

#![allow(clippy::module_inception)]

//! Merge request-related API endpoints
//!
//! These endpoints are used for querying and modifying merge requests.

mod merge_request;
mod merge_requests;

pub use self::merge_request::ApproverIds;
pub use self::merge_request::Assignee;
pub use self::merge_request::MergeRequestMilestone;
pub use self::merge_request::MergeRequestOrderBy;
pub use self::merge_request::MergeRequestScope;
pub use self::merge_request::MergeRequestSearchScope;
pub use self::merge_request::MergeRequestState;
pub use self::merge_request::MergeRequestView;

pub use self::merge_requests::MergeRequests;
pub use self::merge_requests::MergeRequestsBuilder;
pub use self::merge_requests::MergeRequestsBuilderError;
